// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CloundpunkClone/CloundpunkCloneGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCloundpunkCloneGameModeBase() {}
// Cross Module References
	CLOUNDPUNKCLONE_API UClass* Z_Construct_UClass_ACloundpunkCloneGameModeBase_NoRegister();
	CLOUNDPUNKCLONE_API UClass* Z_Construct_UClass_ACloundpunkCloneGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_CloundpunkClone();
// End Cross Module References
	void ACloundpunkCloneGameModeBase::StaticRegisterNativesACloundpunkCloneGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_ACloundpunkCloneGameModeBase_NoRegister()
	{
		return ACloundpunkCloneGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ACloundpunkCloneGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACloundpunkCloneGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CloundpunkClone,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACloundpunkCloneGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "CloundpunkCloneGameModeBase.h" },
		{ "ModuleRelativePath", "CloundpunkCloneGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACloundpunkCloneGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACloundpunkCloneGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACloundpunkCloneGameModeBase_Statics::ClassParams = {
		&ACloundpunkCloneGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ACloundpunkCloneGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ACloundpunkCloneGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACloundpunkCloneGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACloundpunkCloneGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACloundpunkCloneGameModeBase, 813932329);
	template<> CLOUNDPUNKCLONE_API UClass* StaticClass<ACloundpunkCloneGameModeBase>()
	{
		return ACloundpunkCloneGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACloundpunkCloneGameModeBase(Z_Construct_UClass_ACloundpunkCloneGameModeBase, &ACloundpunkCloneGameModeBase::StaticClass, TEXT("/Script/CloundpunkClone"), TEXT("ACloundpunkCloneGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACloundpunkCloneGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
