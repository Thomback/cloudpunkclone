// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CLOUNDPUNKCLONE_CloundpunkCloneGameModeBase_generated_h
#error "CloundpunkCloneGameModeBase.generated.h already included, missing '#pragma once' in CloundpunkCloneGameModeBase.h"
#endif
#define CLOUNDPUNKCLONE_CloundpunkCloneGameModeBase_generated_h

#define CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_SPARSE_DATA
#define CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_RPC_WRAPPERS
#define CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACloundpunkCloneGameModeBase(); \
	friend struct Z_Construct_UClass_ACloundpunkCloneGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ACloundpunkCloneGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CloundpunkClone"), NO_API) \
	DECLARE_SERIALIZER(ACloundpunkCloneGameModeBase)


#define CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesACloundpunkCloneGameModeBase(); \
	friend struct Z_Construct_UClass_ACloundpunkCloneGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ACloundpunkCloneGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CloundpunkClone"), NO_API) \
	DECLARE_SERIALIZER(ACloundpunkCloneGameModeBase)


#define CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACloundpunkCloneGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACloundpunkCloneGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACloundpunkCloneGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACloundpunkCloneGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACloundpunkCloneGameModeBase(ACloundpunkCloneGameModeBase&&); \
	NO_API ACloundpunkCloneGameModeBase(const ACloundpunkCloneGameModeBase&); \
public:


#define CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACloundpunkCloneGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACloundpunkCloneGameModeBase(ACloundpunkCloneGameModeBase&&); \
	NO_API ACloundpunkCloneGameModeBase(const ACloundpunkCloneGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACloundpunkCloneGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACloundpunkCloneGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACloundpunkCloneGameModeBase)


#define CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_12_PROLOG
#define CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_SPARSE_DATA \
	CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_RPC_WRAPPERS \
	CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_INCLASS \
	CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_SPARSE_DATA \
	CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CLOUNDPUNKCLONE_API UClass* StaticClass<class ACloundpunkCloneGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CloundpunkClone_Source_CloundpunkClone_CloundpunkCloneGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
