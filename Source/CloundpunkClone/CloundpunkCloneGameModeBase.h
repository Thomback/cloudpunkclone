// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CloundpunkCloneGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CLOUNDPUNKCLONE_API ACloundpunkCloneGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
